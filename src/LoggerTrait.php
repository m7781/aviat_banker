<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2021  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.0.0
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker;

use Psr\Log\{
	LoggerAwareTrait,
	LoggerInterface,
	LogLevel,
	NullLogger
};

/**
 * Trait for keeping track of logger objects
 */
trait LoggerTrait {

	use LoggerAwareTrait;

	/**
	 * Return the existing logger instance or
	 * a NullLogger, if no instance set
	 *
	 * @return LoggerInterface
	 */
	protected function getLogger(): LoggerInterface
	{
		if ($this->logger === NULL)
		{
			$this->logger = new NullLogger();
		}
		return $this->logger;
	}

	/**
	 * Set a logger to keep track of errors
	 *
	 * @param LoggerInterface $logger
	 * @return self
	 */
	public function setLogger(LoggerInterface $logger): self
	{
		$this->logger = $logger;

		// Set the logger for the current driver too
		if (isset($this->driver))
		{
			$this->driver->setLogger($logger);
		}

		return $this;
	}
}